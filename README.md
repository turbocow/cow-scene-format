# Cow Scene Format

This format is been developed for usage in scene rendering for lab4 in CG course.

## Formal description

The format contains several fields for rendering. 
- It may contain both primitive objects(like cubes or spheres) and comlex objects consisting of triangles. The objects may be defined in the config or referenced by path from a directory.
- Every object has it's own transformation matrix(or several matrixes).
- Also every object has it's own material.
- The Scene may have several light sources and define light for every object seperately.
- The Scene may contain several camera(and screen) positions for rendering, so that the renderer could choose one of them.

## Fields description
All fields with subfields are described in this list.
- Objects: [objects list]
    - Object: [id: number, reference?: string]
    - ...
    - Object
- Transmatrixes: [matrixes list]
    - Transmatrix: [object_id: number,  matrixes?: [main_matrixes list], main_matrix?: list[list[number]]]
    - ...
    - Transmatrix
- Materials: [Materials list]
    - Material: [object_id: number, type: string]
    - ...
    - Material
- Lights: [Lights list]
    - Light:  [object_id?: number, type: string, position?: list[number]]
    - ...
    - Light
- Cameras: [Cameras list]
    - Camera: [id: number, position: list[number]]
    - ...
    - Camera

## Format specifications


## Simple example in JSON

## Simple example in Protobuf

## Example-draft from lection by George
![image.png](./image.png)
